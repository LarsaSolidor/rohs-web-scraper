# RoHS Certifind
A program to make verifying the RoHS status of components, and downloading proof files easier!

## What you get
A folder of RoHS certificate files for the parts you've listed, and a spreadsheet containing the RoHS status of the parts specified.


## Requirements
You'll need to have an excel spreadsheet (.xlsx, or xls) which has three columns:
* ICSI Part Number
* Supplier Part Number
* Description

All three columns must have values for every item added. 

Requires installation of (wkhtmltopdf)[https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox-0.12.5-1.msvc2015-win64.exe]

