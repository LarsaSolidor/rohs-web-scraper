import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog
from pathlib import Path
import rohs_certifind_ui
from scraper import Scraper


class Window(QtWidgets.QMainWindow, rohs_certifind_ui.Ui_MainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.setupUi(self)
        self.actionQuit.setShortcut("Ctrl+Q")
        self.actionQuit.triggered.connect(self.close_application)

        self.supported_suppliers = ['RS', 'Farnell']
        self.supplier = self.supported_suppliers[0]

        self.supplier_pn_btn.clicked.connect(self.browse_for_pns_spreadsheet)
        self.supplier_pn.textChanged[str].connect(self.update_supplier_pns)

        self.supplier_name_cmbo.addItems(self.supported_suppliers)
        self.supplier_name_cmbo.activated.connect(self.get_supplier_name)

        self.cert_storage_folder = Path.home() / Path(r'Desktop/Scraped RoHS Certificates')
        self.cert_location.setText(self.cert_storage_folder.as_posix())
        self.cert_location_btn.clicked.connect(self.browse_for_cert_location)

        self.search_btn.clicked.connect(self.search_and_download)

        self.search_pns = Path('')

    def browse_for_pns_spreadsheet(self):
        file, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileNames()", "Select Part Numbers Spreadsheet",
                                              "Excel Files (*.xlsx *.xls)")
        if file:
            file_path = Path(file)
            self.supplier_pn.setText(file_path.as_posix()) #will trigger update_supplier_pns

    def update_supplier_pns(self):
        try:
            self.search_pns = Path(self.supplier_pn.text())
            if not self.search_pns.suffix in ['.xlsx', '.xlsx"', '.xls', '.xls"']:
                self.search_pns = ''
                self.supplier_pn.setText('Only paths to xlsx or xls accepted')
        except:
            self.supplier_pn.setText('')

    def get_supplier_name(self, index):
        self.supplier = self.supplier_name_cmbo.itemText(index)

    def browse_for_cert_location(self):
        folder = QFileDialog.getExistingDirectory(self, "QFileDialog.getExistingDirectory()",
                                                  "Select Certificate Storage Folder")

        if folder:
            folder_path = Path(folder)
            self.cert_location.setText(folder_path.as_posix())

    def pns_spreadsheet_entered(self):
        if self.supplier_pn.text():
            print(self.search_pns.as_posix(), self.supplier, self.cert_storage_folder.as_posix())
            return True
        else:
            self.label.setText('Please select a spreadsheet containing part numbers to search')
            return False

    def search_and_download(self):
        self.label.setText('Parsing part numbers')

        if not self.pns_spreadsheet_entered():
            return

        scraper = Scraper(self.search_pns, self.supplier, self.cert_storage_folder)

        scraper.search_supplier()
        scraper.save_rohs_status_report()

    @staticmethod
    def close_application():
        sys.exit()


def run():
    app = QtWidgets.QApplication(sys.argv)
    gui = Window()
    gui.show()
    sys.exit(app.exec_())


run()
