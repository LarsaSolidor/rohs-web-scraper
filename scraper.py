import pandas as pd
import requests
from pathlib import Path
import bs4
import pdfkit
import os


class Scraper:

    def __init__(self, search_pns, supplier, cert_folder):
        """
        Download RoHS certificates for a spreadsheet of items to a specificed folder
        :param search_pns: Path() object to spreadsheet containing part numbers to search,
        :param supplier: String of supplier part numbers belong to
        :param cert_folder: Path() object
        """
        os.environ['PATH'] += os.pathsep + r'C:\Program Files\wkhtmltopdf\bin'
        self.search_pns = search_pns
        self.supplier = supplier
        self.cert_storage_path = cert_folder
        self.report_path = str(self.cert_storage_path / '!RoHS Status Report') + '.xlsx'
        self.df = pd.DataFrame()
        self.report_df = pd.DataFrame()

        self.statuses = []

        self.load_spreadsheet()
        self.make_cert_storage_dir()

    def load_spreadsheet(self):
        """
        :return: A list of lists where each list is a row from the supplier's part number spreadsheet
        """
        self.df = pd.read_excel(self.search_pns)
        self.df = self.df.values.tolist()

    def make_cert_storage_dir(self):
        try:
            print(f'Making {self.cert_storage_path.as_posix()} directory if it does not already exist')
            os.mkdir(self.cert_storage_path.as_posix())
        except FileExistsError:
            pass

    def search_supplier(self):
        if self.supplier == 'RS':
            self.search_rs()
        elif self.supplier == 'Farnell':
            self.search_farnell()

    def search_rs(self):

        for item_row in self.df:
            # app.label.setText(f'Searching for {item}')
            print(f'Searching for {item_row}')
            result = 'Not Found'
            try:
                result = self.search_one_rs_item(supplier_pn_row=item_row)
                # self.label.setText(f'Certificate for {item} saved.')
            except:
                pass
            self.statuses.append(result)

        # self.label.setText('Saving report spreadsheet')

    def search_one_rs_item(self, supplier_pn_row):
        """
        :param supplier_pn_row: supplier_pn_row: [icsi_pn, desc, supplier_pn]
        :param cert_storage_path: Path() object
        :return: True if RoHS compliant, False if not RoHS compliant
        """
        icsi_pn, desc, supplier_pn = supplier_pn_row

        supplier_pn = supplier_pn.upper().replace('R', '')

        res = requests.get(f'https://uk.rs-online.com/web/c/?sra=oss&r=t&searchTerm={supplier_pn}')
        res_soup = bs4.BeautifulSoup(res.content, 'lxml')

        try:
            print(f'Checking if {supplier_pn} is RoHS Compliant')
            rohs_compliant = res_soup.select_one('.rohs-certificate-link').text
        except AttributeError:
            print('Attribute Error')
            return None

        if rohs_compliant:
            print('RoHS Compliant!')
            self.save_one_rs_rohs_cert(res_soup, icsi_pn, desc, supplier_pn)
            return True
        else:
            print('Not RoHS Compliant.')
            return False

    def save_one_rs_rohs_cert(self, soup, icsi_pn, desc, supplier_pn):
        certificate = soup.find("div", id="rohsCertificate")

        if not certificate:
            print('Couldn\'t find RoHS certificate')
            return False
        else:
            certificate['style'] = "display:inline"
            for img in certificate.find_all('img'):
                img['src'] = 'https://uk.rs-online.com/' + img['src']
                print(img['src'])

        print('Getting RS CSS information')
        self.get_supplier_css(r'https://uk.rs-online.com/css_09262018_175728/main.css')

        print('Setting PDF options')
        options = {
            'page-size': 'A4',
            'orientation': 'portrait',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
            'dpi': '1200',
            'custom-header': [
                ('Accept-Encoding', 'gzip')
            ],
            'no-outline': None
        }
        print('PDF options set')

        filename = ' - '.join([icsi_pn, desc, supplier_pn])
        print(filename)
        filename = self.remove_illegal_windows_path_characters(filename)

        store_path = str(self.cert_storage_path / filename) + '.pdf'

        try:
            print(f"Creating PDF file: {store_path}")
            pdfkit.from_string(str(certificate), store_path, options=options, css='RS.css')
        except OSError as e:
            print(f'{e}\n\nFailed to save PDF for {filename}')
        print('\n######################################## Finished ########################################\n\n')
        return True
    def remove_illegal_windows_path_characters(self, path):
        path = path.replace("/", "")
        return path

    def get_supplier_css(self, css_url):
        css_filename = f'{self.supplier}.css'
        if not os.path.isfile(css_filename):
            css_data = requests.get(css_url)
            with open(css_filename, 'w') as f:
                f.write(css_data.text)
        else:
            print('CSS file already exists')

    def save_rohs_status_report(self):
        self.report_df = pd.DataFrame(self.df)

        self.report_df.columns = ['ICSI Part Number', 'Description', 'Supplier Part Number']
        self.report_df['RoHS Status'] = self.statuses

        #report_path = str(self.cert_storage_path / '!RoHS Status Report') + '.xlsx'
        #print(f'Saving certificate to {report_path}')
        writer = pd.ExcelWriter(self.report_path)
        self.report_df.to_excel(writer, index=False)
        writer.save()
        #self.label.setText('Complete!')


if __name__ == '__main__':
    pass  # search_pns = Path(r'C:\Users\luke.broad\OneDrive - TTPGroup\Automation\RoHSWebScraper\RS_components.xlsx')
    # supplier = 'RS'
    # cert_folder = Path(r'C:\Users\luke.broad\OneDrive - TTPGroup\Desktop\Certificates')
    #
    # scraper = Scraper(search_pns, supplier, cert_folder)
    #
    # scraper.search_rs(['MFS00000', 'Item Description', '280-537'],
    #                   Path(r'C:\Users\luke.broad\OneDrive - TTPGroup\Desktop\Certificates'))

# #import requests
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# import bs4
#
# proxies = {
#     'http': 'http://ipv4.62.255.234.132.webdefence.global.blackspider.com:8081',
#     'https': 'http://ipv4.62.255.234.132.webdefence.global.blackspider.com:8089',
# }
#
# ListOfFarnellNumbers = (
#     '1021748', '1657931', '1572625', '1759079', '1759265', '1759102', '1850114', '1759130', '1759269', '8737479',
#     '9802258', '1757856', '8150206', '2066855RL', '1890623', '1675499', '9935487', '1498955', '1147914', '1738898',
#     '1170658', '2060039', '1170896', '1838668', '1576602', '1738958', '9330984', '9330933', '2333369', '2395908',
#     '1739722', '1468757', '1003206', '9753885', '9753869', '9754067', '9557237', '1022259', '9632883',
#     '9632867', '9633146', '9632875', '9730940', '1099025', '9633138', '1174017', '9241175', '1141362',
#     '9486828', '9487980', '9346880', '1201274', '9724966', '9726055', '1187956', '197270', '1123102',
#     '1222692', '9633219', '9632948', '1298807', '9632956', '9632964', '1100214', '1099809', '1099823',
#     '1099806', '1103142', '1103877', '9731644', '1100200', '1107854', '1107864', '491330', '9753885',
#     '9753869', '9451943', '317500', '9843612', '1023035', '9803548', '1123152', '146123', '1174026',
#     '1021578', '9633146', '9633138', '8156549', '8734771', '1081235', '9846786', '9556648', '1086574',
#     '1174007', '9332626', '9333576', '9504737', '9346880', '1201274', '9724966', '9726055', '9726357',
#     '9726365', '1099011', '146322', '2309112'
# )
#
# profile = webdriver.FirefoxProfile()
# profile.set_preference("webdriver.load.strategy", "unstable")
#
# driver = webdriver.Firefox(firefox_profile=profile)
#
# for FarnellNo in ListOfFarnellNumbers:
#         # proxies = {
#         #     'http': 'http://ipv4.62.255.234.132.webdefence.global.blackspider.com:8081',
#         #     'https': 'http://ipv4.62.255.234.132.webdefence.global.blackspider.com:8089',
#         # }
#
#         print(("Retrieving manufacturer part number for: %(Search)s" % {"Search": FarnellNo}))
#
#         SearchURL = 'http://uk.farnell.com/search?st=%(Search)s' % {"Search": FarnellNo}
#         print(SearchURL)
#
#         driver.get("http://uk.farnell.com/search?st=%(Search)s" % {"Search": FarnellNo})
#         res = driver.page_source
#
#
#         Soup = bs4.BeautifulSoup(res, "lxml")
#         ManufacturerPartNumber = {}
#         ManufacturerPartNumber[FarnellNo]= Soup.select("dd:nth-of-type(2)") #use nth-of-type instead of whatever it is that firefox gives you
#         #print(Soup.select("dd:nth-of-type(2)"))
#         #print(ManufacturerPartNumber[FarnellNo])
#
#
#         for i in Soup.select("dd:nth-of-type(2)"):
#             print((i.text).strip()) #remove white space
